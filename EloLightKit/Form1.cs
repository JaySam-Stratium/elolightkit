﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Runtime.InteropServices;

using Susi4.APIs;

namespace EloLightKit
{
    public partial class eloLightKitForm : Form
    {
        List<GPIO> selectedGpio = new List<GPIO>();

        public class GPIO : IEquatable<GPIO>
        {
            public int ID;
            public string label;

            public GPIO(int ID, string label)
            {
                this.ID = ID;
                this.label = label;
            }

            public override string ToString()
            {
                return this.label;
            }

            public override bool Equals(object obj)
            {
                if (obj == null) return false;
                GPIO objAsPart = obj as GPIO;
                if (objAsPart == null) return false;
                else return Equals(objAsPart);
            }

            public override int GetHashCode()
            {
                return ID;
            }

            public bool Equals(GPIO other)
            {
                if (other == null) return false;
                return (this.ID.Equals(other.ID));
            }
        }

        public eloLightKitForm()
        {
            InitializeComponent();

            greenPanel.Visible = true;

            greenPanel.Dock = DockStyle.Bottom;
            redPanel.Dock = DockStyle.Bottom;

            try
            {
                UInt32 Status = SusiLib.SusiLibInitialize();

                if (Status != SusiStatus.SUSI_STATUS_SUCCESS && Status != SusiStatus.SUSI_STATUS_INITIALIZED)
                {
                    exitApp();
                }
                else
                {
                    initAllGpio();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("here" + ex.Message);
                exitApp();
            }
        }

        private void exitApp()
        {
            MessageBox.Show("Cannot initialize lightkit library.");

            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }
        }

        private void initAllGpio()
        {
            string errorMsg = "";

            for (int pinId = 1; pinId <= 3; pinId++)
            {
                UInt32 Status;

                Status = SusiGPIO.SusiGPIOSetLevel((uint) pinId, 1, 1);
                if (Status != SusiStatus.SUSI_STATUS_SUCCESS)
                    errorMsg += String.Format("GPIO Port {0} failed. (0x{1:X8})", pinId, Status);
            }

            if (!String.IsNullOrEmpty(errorMsg))
            {
                MessageBox.Show(errorMsg);
            }
        }

        private void greenBtn_Click(object sender, EventArgs e)
        {
            if (selectedGpio.Count == 0)
            {
                MessageBox.Show("No GPIO pin selected.");
                return;
            }

            greenPanel.Visible = false;
            redPanel.Visible = true;

            changeLightKitState(0); // set light kit to red
        }

        private void redBtn_Click(object sender, EventArgs e)
        {
            if (selectedGpio.Count == 0)
            {
                MessageBox.Show("No GPIO pin selected.");
                return;
            }

            redPanel.Visible = false;
            greenPanel.Visible = true;

            changeLightKitState(1); // set light kit to green
        }

        private void changeLightKitState(uint state)
        {
            string errorMsg = "";

            for (int indx = 0; indx < selectedGpio.Count; indx++)
            {
                UInt32 Status;

                Status = SusiGPIO.SusiGPIOSetLevel((uint)selectedGpio[indx].ID, 1, state);
                if (Status != SusiStatus.SUSI_STATUS_SUCCESS)
                    errorMsg += String.Format("GPIO Port {0} failed. (0x{1:X8})", selectedGpio[indx].ID, Status);
            }

            if (!String.IsNullOrEmpty(errorMsg))
            {
                MessageBox.Show(errorMsg);
            }
        }

        private void gpioOne_CheckedChanged(object sender, EventArgs e)
        {
            GPIO obj = new GPIO(1, gpioOne.Text);
            if (gpioOne.Checked)
            {
                selectedGpio.Add(obj);
            }
            else
            {
                selectedGpio.Remove(obj);
            }
        }

        private void gpioTwo_CheckedChanged(object sender, EventArgs e)
        {
            GPIO obj = new GPIO(2, gpioTwo.Text);
            if (gpioTwo.Checked)
            {
                selectedGpio.Add(obj);
            }
            else
            {
                selectedGpio.Remove(obj);
            }
        }

        private void gpioThree_CheckedChanged(object sender, EventArgs e)
        {
            GPIO obj = new GPIO(3, gpioThree.Text);
            if (gpioThree.Checked)
            {
                selectedGpio.Add(obj);
            }
            else
            {
                selectedGpio.Remove(obj);
            }
        }
    }
}
