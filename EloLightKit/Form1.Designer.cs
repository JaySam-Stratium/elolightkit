﻿namespace EloLightKit
{
    partial class eloLightKitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(eloLightKitForm));
            this.greenPanel = new System.Windows.Forms.Panel();
            this.greenBtn = new System.Windows.Forms.PictureBox();
            this.greenBg = new System.Windows.Forms.PictureBox();
            this.redPanel = new System.Windows.Forms.Panel();
            this.redBtn = new System.Windows.Forms.PictureBox();
            this.redBg = new System.Windows.Forms.PictureBox();
            this.gpioOne = new System.Windows.Forms.CheckBox();
            this.gpioTwo = new System.Windows.Forms.CheckBox();
            this.gpioThree = new System.Windows.Forms.CheckBox();
            this.greenPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.greenBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.greenBg)).BeginInit();
            this.redPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.redBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.redBg)).BeginInit();
            this.SuspendLayout();
            // 
            // greenPanel
            // 
            this.greenPanel.Controls.Add(this.greenBtn);
            this.greenPanel.Controls.Add(this.greenBg);
            this.greenPanel.Location = new System.Drawing.Point(12, 40);
            this.greenPanel.Name = "greenPanel";
            this.greenPanel.Size = new System.Drawing.Size(195, 408);
            this.greenPanel.TabIndex = 8;
            this.greenPanel.Visible = false;
            // 
            // greenBtn
            // 
            this.greenBtn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.greenBtn.Image = global::EloLightKit.Properties.Resources.btn_book;
            this.greenBtn.InitialImage = global::EloLightKit.Properties.Resources.btn_book;
            this.greenBtn.Location = new System.Drawing.Point(0, 310);
            this.greenBtn.Name = "greenBtn";
            this.greenBtn.Size = new System.Drawing.Size(195, 98);
            this.greenBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.greenBtn.TabIndex = 1;
            this.greenBtn.TabStop = false;
            this.greenBtn.Click += new System.EventHandler(this.greenBtn_Click);
            // 
            // greenBg
            // 
            this.greenBg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.greenBg.Image = global::EloLightKit.Properties.Resources.bg_green;
            this.greenBg.InitialImage = global::EloLightKit.Properties.Resources.bg_green;
            this.greenBg.Location = new System.Drawing.Point(0, 0);
            this.greenBg.Name = "greenBg";
            this.greenBg.Size = new System.Drawing.Size(195, 408);
            this.greenBg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.greenBg.TabIndex = 0;
            this.greenBg.TabStop = false;
            // 
            // redPanel
            // 
            this.redPanel.Controls.Add(this.redBtn);
            this.redPanel.Controls.Add(this.redBg);
            this.redPanel.Location = new System.Drawing.Point(237, 40);
            this.redPanel.Name = "redPanel";
            this.redPanel.Size = new System.Drawing.Size(200, 408);
            this.redPanel.TabIndex = 9;
            this.redPanel.Visible = false;
            // 
            // redBtn
            // 
            this.redBtn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.redBtn.Image = global::EloLightKit.Properties.Resources.btn_end;
            this.redBtn.InitialImage = global::EloLightKit.Properties.Resources.btn_end;
            this.redBtn.Location = new System.Drawing.Point(0, 310);
            this.redBtn.Name = "redBtn";
            this.redBtn.Size = new System.Drawing.Size(200, 98);
            this.redBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.redBtn.TabIndex = 1;
            this.redBtn.TabStop = false;
            this.redBtn.Click += new System.EventHandler(this.redBtn_Click);
            // 
            // redBg
            // 
            this.redBg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.redBg.Image = global::EloLightKit.Properties.Resources.bg_red;
            this.redBg.InitialImage = global::EloLightKit.Properties.Resources.bg_red;
            this.redBg.Location = new System.Drawing.Point(0, 0);
            this.redBg.Name = "redBg";
            this.redBg.Size = new System.Drawing.Size(200, 408);
            this.redBg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.redBg.TabIndex = 0;
            this.redBg.TabStop = false;
            // 
            // gpioOne
            // 
            this.gpioOne.AutoSize = true;
            this.gpioOne.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.gpioOne.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpioOne.Location = new System.Drawing.Point(12, 12);
            this.gpioOne.Name = "gpioOne";
            this.gpioOne.Size = new System.Drawing.Size(123, 24);
            this.gpioOne.TabIndex = 10;
            this.gpioOne.Text = "GPIO Port 1";
            this.gpioOne.UseVisualStyleBackColor = true;
            this.gpioOne.CheckedChanged += new System.EventHandler(this.gpioOne_CheckedChanged);
            // 
            // gpioTwo
            // 
            this.gpioTwo.AutoSize = true;
            this.gpioTwo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.gpioTwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpioTwo.Location = new System.Drawing.Point(141, 12);
            this.gpioTwo.Name = "gpioTwo";
            this.gpioTwo.Size = new System.Drawing.Size(123, 24);
            this.gpioTwo.TabIndex = 11;
            this.gpioTwo.Text = "GPIO Port 2";
            this.gpioTwo.UseVisualStyleBackColor = true;
            this.gpioTwo.CheckedChanged += new System.EventHandler(this.gpioTwo_CheckedChanged);
            // 
            // gpioThree
            // 
            this.gpioThree.AutoSize = true;
            this.gpioThree.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.gpioThree.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpioThree.Location = new System.Drawing.Point(270, 12);
            this.gpioThree.Name = "gpioThree";
            this.gpioThree.Size = new System.Drawing.Size(123, 24);
            this.gpioThree.TabIndex = 12;
            this.gpioThree.Text = "GPIO Port 3";
            this.gpioThree.UseVisualStyleBackColor = true;
            this.gpioThree.CheckedChanged += new System.EventHandler(this.gpioThree_CheckedChanged);
            // 
            // eloLightKitForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 448);
            this.Controls.Add(this.gpioThree);
            this.Controls.Add(this.gpioTwo);
            this.Controls.Add(this.gpioOne);
            this.Controls.Add(this.redPanel);
            this.Controls.Add(this.greenPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "eloLightKitForm";
            this.Text = "Elo Light Kit";
            this.greenPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.greenBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.greenBg)).EndInit();
            this.redPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.redBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.redBg)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel greenPanel;
        private System.Windows.Forms.PictureBox greenBtn;
        private System.Windows.Forms.PictureBox greenBg;
        private System.Windows.Forms.Panel redPanel;
        private System.Windows.Forms.PictureBox redBtn;
        private System.Windows.Forms.PictureBox redBg;
        private System.Windows.Forms.CheckBox gpioOne;
        private System.Windows.Forms.CheckBox gpioTwo;
        private System.Windows.Forms.CheckBox gpioThree;
    }
}

